#!/bin/sh

# Setup Git remote
git remote add heroku "git@heroku.com:$HEROKU_APP.git"

# Deploy via Git
git push heroku HEAD:refs/heads/master


curl -n -X POST https://api.heroku.com/apps/$HEROKU_APP/dynos \
-H "Accept: application/vnd.heroku+json; version=3" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer $HEROKU_KEY" \
-d "{ 
\"attach\":false,
\"env\": { \"APP_ENV\":\"local\"},
\"command\":\"php artisan migrate\"
} "

